<?php

/**
 * @file
 * Hook implementations for this module.
 */

use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use JamesHeinrich\GetID3\GetID3;

/**
 * Implements hook_views_rss_item_elements_alter().
 */
function views_rss_media_getid3_views_rss_item_elements_alter(&$elements) {
  $elements['views_rss_media']['media:content']['preprocess functions'][] = 'views_rss_media_getid3_preprocess_media_content';
  $elements['views_rss_media']['media:thumbnail']['preprocess functions'][] = 'views_rss_media_getid3_preprocess_media_thumbnail';
}

/**
 * Preprocess function for item <media:content> element.
 */
function views_rss_media_getid3_preprocess_media_content(&$variables) {
  foreach ($variables['raw']['items'] as $index => $item) {
    $file = NULL;
    // File fields.
    if (!empty($item['rendered']['#file'])) {
      $file = $item['rendered']['#file'];
    }
    // Image fields.
    if (!empty($item['rendered']['#item']->entity)) {
      $file = $item['rendered']['#item']->entity;
    }

    if ($file instanceof FileInterface) {
      if ($image_style_name = $item['rendered']['#image_style']) {
        /** @var \Drupal\image\ImageStyleInterface $image_style */
        $image_style = ImageStyle::load($image_style_name);
        $uri = $image_style->buildUri($file->getFileUri());
      }
      // Normal image size.
      else {
        $uri = $file->getFileUri();
      }
      $realpath = \Drupal::service('file_system')->realpath($uri);
      $id3_info = (new GetID3())->analyze($realpath);

      // Video details.
      if (!empty($id3_info['video']['frame_rate'])) {
        $variables['elements'][$index]['attributes']['framerate'] = $id3_info['video']['frame_rate'];
      }
      if (!empty($id3_info['video']['bitrate'])) {
        $variables['elements'][$index]['attributes']['bitrate'] = $id3_info['video']['bitrate'];
      }
      if (!empty($id3_info['video']['resolution_x'])) {
        $variables['elements'][$index]['attributes']['width'] = $id3_info['video']['resolution_x'];
      }
      if (!empty($id3_info['video']['resolution_y'])) {
        $variables['elements'][$index]['attributes']['height'] = $id3_info['video']['resolution_y'];
      }

      // Audio details.
      if (!empty($id3_info['audio']['bitrate'])) {
        $variables['elements'][$index]['attributes']['bitrate'] = $id3_info['audio']['bitrate'];
      }
      if (!empty($id3_info['audio']['sample_rate'])) {
        $variables['elements'][$index]['attributes']['samplingrate'] = $id3_info['audio']['sample_rate'] / 1000;
      }
      if (!empty($id3_info['audio']['channels'])) {
        $variables['elements'][$index]['attributes']['channels'] = $id3_info['audio']['channels'];
      }

      // Misc details.
      if (!empty($id3_info['playtime_seconds'])) {
        $variables['elements'][$index]['attributes']['duration'] = (int) $id3_info['playtime_seconds'];
      }
    }
  }
}

/**
 * Preprocess function for item <media:thumbnail> element.
 */
function views_rss_media_getid3_preprocess_media_thumbnail(&$variables) {
  foreach ($variables['raw']['items'] as $index => $item) {
    $file = NULL;
    // File fields.
    if (!empty($item['rendered']['#file'])) {
      $file = $item['rendered']['#file'];
    }
    // Image fields.
    if (!empty($item['rendered']['#item']->entity)) {
      $file = $item['rendered']['#item']->entity;
    }
    if ($file instanceof FileInterface) {
      // Image style is defined, need to link to resized version.
      if ($image_style_name = $item['rendered']['#image_style']) {
        /** @var \Drupal\image\ImageStyleInterface $image_style */
        $image_style = ImageStyle::load($image_style_name);
        $uri = $image_style->buildUri($file->getFileUri());
      }
      // Normal image size.
      else {
        $uri = $file->getFileUri();
      }

      $realpath = \Drupal::service('file_system')->realpath($uri);
      $id3_info = (new GetID3())->analyze($realpath);

      if (!empty($id3_info['video']['resolution_x'])) {
        $variables['elements'][$index]['attributes']['width'] = $id3_info['video']['resolution_x'];
      }
      if (!empty($id3_info['video']['resolution_y'])) {
        $variables['elements'][$index]['attributes']['height'] = $id3_info['video']['resolution_y'];
      }
    }
  }
}
