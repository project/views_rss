# Advanced Views RSS Feed: Media getID3

## Requirements

This module requires the getID3 library, specifically v2:

    composer require "james-heinrich/getid3:^2"

At the time of writing there isn't a stable release of the v2 branch available,
so use this command instead:

    composer require "james-heinrich/getid3:^2.0@beta"

or this:

    composer require "james-heinrich/getid3:2.0.x-dev@dev"
